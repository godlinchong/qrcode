import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'super-secret'
    DB_HOST = '148.70.107.5'
    DB_USER = 'root'
    DB_PASSWD = '123456'
    DB_DATABASE = 'qrcode'
    ITEMS_PER_PAGE = 10
    JWT_AUTH_URL_RULE = '/api/auth'
    URL= "http://localhost:5555/"

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    PRODUCTION = True


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
