# -*- coding: utf-8 -*-
from os import path

from peewee import MySQLDatabase, Model, CharField, BooleanField, IntegerField, DateTimeField, DoubleField
import json
from werkzeug.security import check_password_hash
from flask_login import UserMixin
from app import login_manager
from conf.config import config
import os
import xlrd
from MyQR import myqr

cfg = config[os.getenv('FLASK_CONFIG') or 'default']

db = MySQLDatabase(host=cfg.DB_HOST, user=cfg.DB_USER, passwd=cfg.DB_PASSWD, database=cfg.DB_DATABASE)


class BaseModel(Model):
    class Meta:
        database = db

    def __str__(self):
        r = {}
        for k in self._data.keys():
            try:
                r[k] = str(getattr(self, k))
            except:
                r[k] = json.dumps(getattr(self, k))
        # return str(r)
        return json.dumps(r, ensure_ascii=False)


# 管理员工号
class User(UserMixin, BaseModel):
    username = CharField()  # 用户名
    password = CharField()  # 密码
    fullname = CharField()  # 真实性名
    email = CharField()  # 邮箱
    phone = CharField()  # 电话
    status = BooleanField(default=True)  # 生效失效标识

    def verify_password(self, raw_password):
        return check_password_hash(self.password, raw_password)


# 通知人配置
class QrCode(BaseModel):
    build_company = CharField(default="湖北交投蕲春西高速公路有限公司")  # 建设单位
    design_company = CharField(default="中交公路规划设计院有限公司")  # 设计单位
    check_company = CharField(default="湖北省公路工程咨询监理中心")  # 监理单位
    operation_company = CharField(default="湖北长江路桥股份有限公司")  # 施工单位
    order = IntegerField()  # 排序
    program_location = CharField()  # 工程位置
    # design_code = CharField()  # 设计标识
    pour_time = CharField()  # 浇筑日期
    # standup_time = CharField()  # 抗压日期
    strength = DoubleField()  # 强度
    qr_file_name = CharField(default='')  # 二维码文件的地址


@login_manager.user_loader
def load_user(user_id):
    return User.get(User.id == int(user_id))


# 建表
def create_table():
    db.connect()
    db.create_tables([QrCode, User])


if __name__ == '__main__':
    create_table()
    root_path = path.dirname(path.abspath(__file__))
    root_path1 = root_path[:len(root_path) - 1]
    xld_file = path.join(path.dirname(root_path1), 'conf/qr.xls')
    print(xld_file)
    data = xlrd.open_workbook(xld_file)

    sheet1 = data.sheets()[0]
    nrows = sheet1.nrows
    print('表格总行数', nrows)
    cur_count = 3
    # while cur_count < nrows:
    #     row_values = sheet1.row_values(cur_count)
    #     cur_count += 1
    #     pour_time=xlrd.xldate.xldate_as_datetime(row_values[2], 0).strftime("%Y-%m-%d")
    #     qr = QrCode(order=cur_count, program_location=row_values[1], pour_time=pour_time,strength=float(row_values[3]))
    #     qr.save()

    # 制作二维码
    url = "http://wslio.club/qr/"
    qr_codes = QrCode.select()
    dir = path.join(path.dirname(root_path1), 'app/static/qr')
    for qr_code in qr_codes:
        myqr.run(words='{}{}'.format(url, qr_code.id), save_dir=dir,
                 save_name="{}.jpg".format(qr_code.id))
        qr=QrCode(qr_file_name="qr/{}.jpg".format(qr_code.id))
        qr.id=qr_code.id
        qr.save()

