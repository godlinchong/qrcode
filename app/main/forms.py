from flask_wtf import FlaskForm
from wtforms import FloatField,StringField, SubmitField, BooleanField, PasswordField, SelectField, TextAreaField, HiddenField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo


class QrCodeForm(FlaskForm):
    build_company = StringField('建设单位', validators=[DataRequired(message='不能为空'), Length(0, 64, message='长度不正确')])
    design_company = StringField('设计单位', validators=[DataRequired(message='不能为空'), Length(0, 64, message='长度不正确')])
    check_company = StringField('监理单位', validators=[DataRequired(message='不能为空'), Length(0, 64, message='长度不正确')])
    operation_company = StringField('施工单位', validators=[DataRequired(message='不能为空'), Length(0, 64, message='长度不正确')])
    program_location = StringField('工程位置',
                              validators=[DataRequired(message='不能为空'), Length(0, 64, message='长度不正确')])
    pour_time = StringField('浇筑时间', validators=[DataRequired(message='不能为空'), Length(0, 64, message='长度不正确')])
    strength = FloatField('强度结果', validators=[DataRequired(message='不能为空')])
    submit = SubmitField('提交')
